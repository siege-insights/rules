## Introduction
Our teamspeak is meant to be an open place for people who enjoy playing siege. Of course gamers from other titles are also welcome, but our priority is siege.

## Rules to follow
We only ask you to
 
1. Be nice to other people (no one likes it to be insulted)
1. Have a good to read name - If possible `Nickname | Real first name`
1. Stay organized - whenever you start playing as a party / lobby, make sure to switch channel. Of course you can stay in the channel if everybody in there is ok with it.
1. We have specialized channels, which are exclusive available for Siege players. - These are guarded by permissions and bots.

# Account linking
To provide a great and easy way for you to find other players, we ask you to link your Rainbow Six: Siege account to your teamspeak account.
Just switch to the `Request account linking` channel and our bot will message you.
